# DISCLAIMER #

This is a Z-Team project for a Hackathon at the "[Android Academy Moscow](https://habr.com/post/420573/), Android Fundamentals" 2018.

The project may contains wrong patterns, bad practice, code examples which are not corresponds with S.O.L.I.D. principles and/or with a Clean architecture.
Etc. Please don't tell Uncle Bob.
Also this may contains some piece of incomprehensible humor.
Please use the code "as is" and don't ask questons.

### What is this repository for? ###

* It's to demonstrate team mates skills after the Academy's cource.
* Current version is a "1.0.0.1" and it means that the project completed at about 1%. Don't try to find any logic in numbers. It's just a revision number.

### Rules we are decided to realize in the project (a Contract) ###

* Here is a description [url](about:blank)
> Imho
> Lmao
> Rofl
> Lol

### How do I get set up? ###

* Step-by-step
* The trick is _be tricky_

### Contribution guidelines ###

* We're not waiting for any contributions
* You could do a code review on your own
* Sorry, no tests are available

### Who do I talk to? ###

* Original repo owner is a [Z-Team](https://bitbucket.org/webanimal/z-team-hack-the-world/src)
* You may try to find us @ android-academy-msk.slack.com, z-team