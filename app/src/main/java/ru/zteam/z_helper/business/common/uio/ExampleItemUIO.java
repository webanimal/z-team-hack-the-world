package ru.zteam.z_helper.business.common.uio;

import android.support.annotation.NonNull;

/**
 * UI Object
 */
public class ExampleItemUIO {

    private final int id;
    private final String title;
    private final String text;
    private final String date;

    public ExampleItemUIO(int id, @NonNull final String title, @NonNull final String text, @NonNull final String date) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }
}
