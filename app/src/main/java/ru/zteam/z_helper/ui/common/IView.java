package ru.zteam.z_helper.ui.common;

import android.arch.lifecycle.LifecycleOwner;

/**
 * Marker interface with LifecycleOwner
 */
public interface IView extends LifecycleOwner {
}
