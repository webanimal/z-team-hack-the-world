package ru.zteam.z_helper.ui.common;

import android.arch.lifecycle.LifecycleObserver;

/**
 * Marker interface with LifecycleObserver
 */
public interface IPresenter extends LifecycleObserver {
}
