package ru.zteam.z_helper.ui.features.mainfeature;

import android.support.annotation.NonNull;
import android.view.Menu;

import ru.zteam.z_helper.ui.common.IPresenter;

public interface IMainPresenter extends IPresenter {

    /**
     * To attach a View to a Presenter. To listen to Presenter's commands.
     */
    void bindView(@NonNull IMainView mainViewImpl);


    /**
     * View asks a Presenter to load a data for a chosen name.
     */
    void loadExample(@NonNull String name);

//    Это то, что на главном экране будет помещаться в нижнее меню с табами
//    Предварительно, содержат: айдишник меню, название
//    @NonNull
//    void getCategories();

//    void getCatalogItemsForCategory(int categoryId)

//    void getRecipes(@NonNull List<IngredientUIO>)
}
