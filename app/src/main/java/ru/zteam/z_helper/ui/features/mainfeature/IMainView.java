package ru.zteam.z_helper.ui.features.mainfeature;

import android.support.annotation.NonNull;
import android.view.Menu;

import ru.zteam.z_helper.ui.common.IView;


/**
 * Don't forget to check "if (hasView()) {...}" inside every method
 */
public interface IMainView extends IView {

//    void onCategoriesUpdated(@NonNull List<CategoryUIO> categories);

//    void onCatalogItemsForCategoryUpdated(int categoryId, @NonNull List<IngredientUIO> ingredients);

//    void onRecipesUpdated(@NonNull List<Recipes> recipes);

    void onExampleUpdated(@NonNull String data);

    void onError(@NonNull String message);

    void onEmptyData(@NonNull String message);
}
