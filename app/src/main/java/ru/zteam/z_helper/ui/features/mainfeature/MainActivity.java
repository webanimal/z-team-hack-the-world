package ru.zteam.z_helper.ui.features.mainfeature;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import ru.zteam.z_helper.R;
import ru.zteam.z_helper.ui.common.BaseActivity;
import ru.zteam.z_helper.utils.Application;
import ru.zteam.z_helper.utils.DrawableUtils;

public class MainActivity extends BaseActivity implements IMainView {

    //==============================================================================================
    // Static
    //==============================================================================================

    private final static int LAYOUT_MAIN = R.layout.activity_main;
    private final static int LAYOUT_MENU = R.menu.menu_main;


    //==============================================================================================
    // Widgets
    //==============================================================================================

    private TextView titleView;


    //==============================================================================================
    // Fields
    //==============================================================================================

    private final String nowhereName = "requestId";


    //==============================================================================================
    // Listeners
    //==============================================================================================

    // Init listeners here


    //==============================================================================================
    // Activity callbacks
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT_MAIN);
        bindLifecycle();

        setupUI();
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupUX(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getPresenter().loadExample(nowhereName);
//        getPresenter().getCategories();
    }

    @Override
    protected void onStop() {
        setupUX(true);

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(LAYOUT_MENU, menu);
        setupMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int actionId = item.getItemId();
        switch (actionId) {
            case R.id.action_main_about:
                Toast.makeText(this, R.string.main_action_about_title, Toast.LENGTH_SHORT).show();
                break;

            case R.id.action_main_settings:
                Toast.makeText(this, R.string.main_action_settings_title, Toast.LENGTH_SHORT).show();
                break;
        }

        return false;
    }


    //==============================================================================================
    // IMainView callbacks
    //==============================================================================================

//    @Override
//    public onCategoriesUpdated(@NonNull List<CategoryUIO> categories) {
//        for (CategoryUIO category : categories) {
//             getCatalogItemsForCategory(int categoryId);
//        }
//    }

//    @Override
//    public void onCatalogItemsForCategoryUpdated(int categoryId, @NonNull List<IngredientUIO> ingredients) {
//        // update catalog adapters here
//    }

//    @Override
//    public onRecipesUpdated(@NonNull List<Recipes> recipes) {
        // update second screen adapter here
//    }

    @Override
    public void onExampleUpdated(@NonNull final String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                titleView.setText(data);
            }
        }, 2000);
    }

    @Override
    public void onError(@NonNull String message) {
    }

    @Override
    public void onEmptyData(@NonNull String message) {
    }


    //==============================================================================================
    // Protected methods
    //==============================================================================================

    @Override
    protected void bindLifecycle() {
        getLifecycle().addObserver(getPresenter());
        getPresenter().bindView(this);
    }

    @Override
    protected IMainPresenter getPresenter() {
        return presenters().main();
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================

    private void setupUI() {
        setupTitle();
        findViews();
        buildRecycler();
        setupViews(nowhereName);
    }

    private void setupTitle() {
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(getString(R.string.activity_main_title));
    }

    private void findViews() {
        titleView = findViewById(R.id.cardTitle);
    }

    private void buildRecycler() {
        // create an ArrayList
        // call Presenter to create dataset for adapter
        // create Adapter
        // set adapter to recycler
        // receive callback from presenter with dataset
        // update and notify adapter with DiffUtils
    }

    private void setupViews(@NonNull String message) {
        titleView.setText(message);
    }

    private void setupMenu(@NonNull final Menu menu) {
        final MenuItem itemSettings = menu.findItem(R.id.action_main_settings);
        final MenuItem itemAbout = menu.findItem(R.id.action_main_about);
        final int colorId
                = Application.provides().context().getResources().getColor(R.color.colorWhite);

        DrawableUtils.setMenuIconTintColor(itemAbout, colorId);
        DrawableUtils.setMenuIconTintColor(itemSettings, colorId);
    }

    private void setupUX(boolean setListener) {
        // attach listeners to views here
        // view.setListener(setListener ? listener : null)

        // getRecipes(@NonNull List<IngredientUIO>)
    }
}
