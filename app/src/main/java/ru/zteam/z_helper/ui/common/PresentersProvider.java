package ru.zteam.z_helper.ui.common;

import android.support.annotation.NonNull;

import ru.zteam.z_helper.ui.features.mainfeature.IMainPresenter;
import ru.zteam.z_helper.ui.features.mainfeature.MainPresenter;

public class PresentersProvider {

    //==============================================================================================
    // Static
    //==============================================================================================

    private static PresentersProvider instance;
    public static PresentersProvider get() {
        if (instance == null) {
            instance = new PresentersProvider();
        }

        return instance;
    }


    //==============================================================================================
    // Fields
    //==============================================================================================

    private final IMainPresenter mainPresenterImpl;


    //==============================================================================================
    // Constructor
    //==============================================================================================

    private PresentersProvider() {
        this.mainPresenterImpl = new MainPresenter();
    }


    //==============================================================================================
    // Getters and Setters
    //==============================================================================================

    @NonNull
    public IMainPresenter main() {
        return mainPresenterImpl;
    }
}
