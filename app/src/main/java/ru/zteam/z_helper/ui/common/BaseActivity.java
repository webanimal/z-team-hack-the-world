package ru.zteam.z_helper.ui.common;

import android.support.v7.app.AppCompatActivity;

import ru.zteam.z_helper.utils.Application;

/**
 * You have to register LifecycleOwner for View which implements IView.
 * Not to a BaseActivity class.
 * Please, implement IView interface into your current view class.
 * See also {@link ru.zteam.z_helper.ui.features.mainfeature.MainActivity}
 */
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Short way to get presenters holden by an Application class.
     * @return a PresentersProvider's instance.
     */
    protected PresentersProvider presenters() {
        return Application.provides().presenters();
    }

    /**
     * This method is intended to standardize binding process of a view to a presenter.
     * Add a LifecycleObserver and bind a View to a Presenter here.
     */
    protected abstract void bindLifecycle();

    /**
     * Provides access to a presenter's commands.
     * Also, this presenter is a lifecycle observer.
     * Obtain the presenter from a {@link #presenters()} here.
     */
    protected abstract IPresenter getPresenter();
}
