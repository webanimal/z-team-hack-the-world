package ru.zteam.z_helper.ui.common;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.zteam.z_helper.utils.logging.Log;

/**
 * You have to bind Views to Presenter which implements IPresenter.
 * Not to a BasePresenter class.
 * Please, implement IPresenter interface into your current presenter class.
 * See also {@link ru.zteam.z_helper.ui.features.mainfeature.MainPresenter}
 */
public abstract class BasePresenter {

    //==============================================================================================
    // Fields
    //==============================================================================================

    private IView viewImpl;
    private boolean isReady;

    private final CompositeDisposable disposables = new CompositeDisposable();


    //==============================================================================================
    // LifecycleObserver callbacks
    //==============================================================================================

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    private void onLifecycleChange(LifecycleOwner source, Lifecycle.Event event) {
        final Lifecycle.State state = source.getLifecycle().getCurrentState();
        switch (state) {
            case STARTED:
            case RESUMED:
                Log.d("state:" + state);
                setReady(true);
                break;
            case INITIALIZED:
            case CREATED:
            case DESTROYED:
                Log.d("state:" + state);
                setReady(false);
                break;
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void clearDisposables() {
        Log.called();
        disposables.clear();
    }


    //==============================================================================================
    // Protected methods
    //==============================================================================================

    /**
     * BasePresenter method.
     * Register a View into a Presenter here to control a View's LifeCycle.
     */
    protected void setView(IView viewImpl) {
        this.viewImpl = viewImpl;
    }

    /**
     * BasePresenter method.
     * Check before call any method on a View.
     */
    protected boolean hasView() {
        return viewImpl != null && isReady;
    }

    /**
     * BasePresenter method.
     * Utilize disposables here.
     * @param d a Disposable instance
     */
    protected void addDisposable(Disposable d) {
        disposables.add(d);
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================

    private void setReady(boolean newState) {
        this.isReady = newState;
    }
}
