package ru.zteam.z_helper.ui.features.mainfeature;

import android.support.annotation.NonNull;

import ru.zteam.z_helper.ui.common.BasePresenter;

public class MainPresenter extends BasePresenter implements IMainPresenter {

    //==============================================================================================
    // Fields
    //==============================================================================================

    private IMainView mainViewImpl = null;


    //==============================================================================================
    // IMainPresenter callbacks
    //==============================================================================================

    @Override
    public void bindView(@NonNull final IMainView mainViewImpl) {
        super.setView(mainViewImpl);
        this.mainViewImpl = mainViewImpl;
    }

    @Override
    public void loadExample(@NonNull final String name) {
        // do some work
        if (hasView()) {
            mainViewImpl.onExampleUpdated(name + " is done");
        }
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================
}
