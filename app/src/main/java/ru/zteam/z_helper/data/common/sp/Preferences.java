package ru.zteam.z_helper.data.common.sp;

import android.content.Context;

import ru.zteam.z_helper.utils.Application;
import ru.zteam.z_helper.utils.PreferencesUtils;

public class Preferences {

    public static void setTutorialShown(boolean value) {
        PreferencesUtils.putValue(appContext(), "activity_tutorial_is_shown", value);
    }

    public static boolean getTutorialShown() {
        return PreferencesUtils.getValue(appContext(), "activity_tutorial_is_shown", false);
    }


    // =============================================================================================

    private Preferences() {}

    private static Context appContext() {
        return Application.provides().context();
    }
}
