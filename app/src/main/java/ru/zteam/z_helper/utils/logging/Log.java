package ru.zteam.z_helper.utils.logging;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ru.zteam.z_helper.BuildConfig;
import ru.zteam.z_helper.utils.Application;

public final class Log {

    //==============================================================================================
    // Static fields
    //==============================================================================================

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({DELIMITER, DEBUG, ERROR, WARN, VERBOSE, INFO})
    public @interface LOG_TYPES {}

    private static final int DELIMITER = 0;
    public static final int DEBUG = 1;
    public static final int ERROR = 2;
    public static final int WARN = 4;
    public static final int VERBOSE = 8;
    public static final int INFO = 16;
    private static int LOG_LEVEL = Log.ERROR | Log.WARN | Log.INFO;

    private static String APP_CODE = "";


    //==============================================================================================
    // Static getters and setters
    //==============================================================================================

    /**
     * To recognize different build variants
     * @param appCode
     */
    public static void setAppCode(@NonNull String appCode) {
        APP_CODE = appCode;
    }

    /**
     * Add a level
     * @param logLevel
     */
    public static void addLogLevel(@LOG_TYPES final int logLevel) {
        LOG_LEVEL |= logLevel;
    }

    /**
     * Override current log levels
     * @param logLevel
     */
    public static void setLogLevel(@LOG_TYPES final int logLevel) {
        LOG_LEVEL = logLevel;
    }


    //==============================================================================================
    // Static public methods
    //==============================================================================================

    public static void d(@Nullable final String message) {
        if ((LOG_LEVEL & Log.DEBUG) > 0) {
            Log.log(DEBUG, message);
        }
    }

    public static void e(@Nullable final String message) {
        if ((LOG_LEVEL & Log.ERROR) > 0) {
            Log.log(ERROR, message);
        }
    }

    public static void e(@Nullable final Throwable e) {
        if (e != null) {
            Log.e(null, e);
        }
    }

    public static void e(@Nullable final String message, Throwable e) {
        if ((LOG_LEVEL & Log.ERROR) > 0) {
            if (message != null) {
                Log.log(ERROR, message);
            }

            if (e != null) {
                Log.log(ERROR, e.getMessage());
            }

            Log.log(ERROR, android.util.Log.getStackTraceString(e));
        }
    }

    public static void w(@Nullable final String message) {
        if ((LOG_LEVEL & Log.WARN) > 0) {
            Log.log(WARN, message);
        }
    }

    public static void v(@Nullable final String message) {
        if ((LOG_LEVEL & Log.VERBOSE) > 0) {
            Log.log(VERBOSE, message);
        }
    }

    public static void i(@Nullable final String message) {
        if ((LOG_LEVEL & Log.INFO) > 0) {
            Log.log(INFO, message);
        }
    }

    //==============================================================================================

    public static void entered() {
        Log.d("entered");
    }

    public static void finished() {
        Log.d("finished");
    }

    public static void finished(String comment) {
        Log.d("finished: " + comment);
    }

    public static void fired() {
        Log.d("fired");
    }

    public static void called() {
        Log.d("called");
    }

    public static void failed() {
        Log.d("failed");
    }

    public static void succeeded() {
        Log.d("succeeded");
    }

    public static void delimiter() {
        Log.log(DELIMITER, "*******");
    }


    //==============================================================================================
    // Static private methods
    //==============================================================================================

    private static void log(@LOG_TYPES final int type, final String message) {
        final String finalMessage = Log.getScope() + Log.normalize(message);
        switch (type) {
            case DELIMITER:
                android.util.Log.i(APP_CODE, "I: " + finalMessage);
                break;

            case DEBUG:
                android.util.Log.d(APP_CODE, "D: " + finalMessage);
                break;

            case ERROR:
                android.util.Log.e(APP_CODE, "E: " + finalMessage);
                break;

            case WARN:
                android.util.Log.w(APP_CODE, "W: " + finalMessage);
                break;

            case VERBOSE:
                android.util.Log.v(APP_CODE, "V: " + finalMessage);
                break;

            case INFO:
                android.util.Log.i(APP_CODE, "I: " + finalMessage);
                break;
        }
    }

    private static String getScope() {
        final StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        StackTraceElement begin = null;
        for (int i = trace.length - 1; i >= 0 ; --i) {
            if (trace[i].getClassName().startsWith(Log.class.getName())) {
                break;
            }

            begin = trace[i];
        }

        String threadName = Thread.currentThread().getName();
        if (TextUtils.isEmpty(threadName)) {
            threadName = "";

        } else {
            threadName += " ";
        }
        threadName += String.valueOf(Thread.currentThread().getId());

        return begin == null ? "" : (begin.getClassName() + "." + begin.getMethodName())
                + "[" + threadName + "]:";
    }

    @NonNull
    private static String normalize(@Nullable final String message) {
        return message == null ? "" : message;
    }


    //==============================================================================================
    // Constructor
    //==============================================================================================

    private Log() {}
}
