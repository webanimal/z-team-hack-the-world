package ru.zteam.z_helper.utils.exceptions;

import java.lang.Thread.UncaughtExceptionHandler;

import ru.zteam.z_helper.utils.logging.Log;

public class UncaughtExceptionHandlerWrapper {

    //==============================================================================================
    // Static
    //==============================================================================================

    private static UncaughtExceptionHandler defaultHandler;

    public static void setNewUncaughtExceptionHandler() {
        defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new ExtendedUncaughtExceptionHandler());
    }


    //==============================================================================================
    // Constructor
    //==============================================================================================

    private UncaughtExceptionHandlerWrapper() {}


    //==============================================================================================
    // Inner class
    //==============================================================================================

    private static class ExtendedUncaughtExceptionHandler implements UncaughtExceptionHandler {

        //==========================================================================================
        // UncaughtExceptionHandler callbacks
        //==========================================================================================

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            handleException(t, e);

            if (defaultHandler != null) {
                defaultHandler.uncaughtException(t, e);
            }
        }


        //==========================================================================================
        // Private methods
        //==========================================================================================

        private void handleException(Thread t, Throwable e) {
            try {
                Log.delimiter();
                Log.e("Uncaught exception handled for thread: " + t.getName());
                Log.e(e);
                Log.delimiter();

            } catch (Exception ignored) {}
        }
    }
}
