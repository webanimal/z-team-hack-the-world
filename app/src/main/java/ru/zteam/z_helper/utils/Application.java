package ru.zteam.z_helper.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import ru.zteam.z_helper.BuildConfig;
import ru.zteam.z_helper.data.common.hardcore.HardcoreProvider;
import ru.zteam.z_helper.ui.common.PresentersProvider;
import ru.zteam.z_helper.utils.exceptions.UncaughtExceptionHandlerWrapper;
import ru.zteam.z_helper.utils.logging.Log;

public class Application extends MultiDexApplication {

    //==============================================================================================
    // Static
    //==============================================================================================

    private static Application instance;

    @NonNull
    public static Application provides() {
        return instance;
    }


    //==============================================================================================
    // Fields
    //==============================================================================================

    private final PresentersProvider presentersProvider;
    private final HardcoreProvider hardcoreProvider;


    //==============================================================================================
    // Constructor
    //==============================================================================================

    public Application() {
        super();
        instance = this;
        UncaughtExceptionHandlerWrapper.setNewUncaughtExceptionHandler();

        Log.setAppCode(BuildConfig.APP_CODE);
        if (BuildConfig.DEBUG) {
            Log.addLogLevel(Log.DEBUG);
        }
        Log.delimiter();
        Log.i("Application started. Version: " + getVersionName());

        // Init fields here
        presentersProvider = PresentersProvider.get();
        hardcoreProvider = HardcoreProvider.get();
    }


    //==============================================================================================
    // Public methods
    //==============================================================================================

    @NonNull
    public Context context() { return instance; }

    @NonNull
    public PresentersProvider presenters() {
        return presentersProvider;
    }

    @NonNull
    public HardcoreProvider hardcore() {
        return hardcoreProvider;
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================

    private String getVersionName() {
        return BuildConfig.FLAVOR + "-" + BuildConfig.VERSION_NAME + "-" + BuildConfig.BUILD_TYPE;
    }
}
