package ru.zteam.z_helper.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class PreferencesUtils {

    public static boolean contains(@NonNull Context context, @NonNull String key) {
        return sharedPreferences(context).contains(key);
    }

    public static boolean getValue(@NonNull Context context, @NonNull String key, boolean defValue) {
        return sharedPreferences(context).getBoolean(key, defValue);
    }

    public static void putValue(@NonNull Context context, @NonNull String key, boolean value) {
        sharedPreferencesEditor(context)
                .putBoolean(key, value)
                .apply();
    }

    public static int getValue(@NonNull Context context, @NonNull String key, int defValue) {
        return sharedPreferences(context).getInt(key, defValue);
    }

    public static void putValue(@NonNull Context context, @NonNull String key, int value) {
        sharedPreferencesEditor(context)
                .putInt(key, value)
                .apply();
    }

    public static float getValue(@NonNull Context context, @NonNull String key, float defValue) {
        return sharedPreferences(context).getFloat(key, defValue);
    }

    public static void putValue(@NonNull Context context, @NonNull String key, float value) {
        sharedPreferencesEditor(context)
                .putFloat(key, value)
                .apply();
    }

    public static double getValue(@NonNull Context context, @NonNull String key, double defValue) {
        return Double.longBitsToDouble(
                sharedPreferences(context).getLong(key, Double.doubleToLongBits(defValue)));
    }

    public static void putValue(@NonNull Context context, @NonNull String key, double value) {
        sharedPreferencesEditor(context)
                .putLong(key, Double.doubleToLongBits(value))
                .apply();
    }

    public static String getValue(@NonNull Context context, @NonNull String key, @NonNull String defValue) {
        return sharedPreferences(context).getString(key, defValue);
    }

    public static void putValue(@NonNull Context context, @NonNull String key, @NonNull String value) {
        sharedPreferencesEditor(context)
                .putString(key, value)
                .apply();
    }


    // =============================================================================================
    // Private methods
    // =============================================================================================

    private static SharedPreferences sharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static SharedPreferences.Editor sharedPreferencesEditor(Context context) {
        return sharedPreferences(context).edit();
    }


    //==============================================================================================

    private PreferencesUtils() {}
}
