package ru.zteam.z_helper.utils;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.Surface;

public class DisplayUtils {

    public static int getDisplayRotation(Activity activity) {
        return activity.getWindowManager().getDefaultDisplay().getRotation();
    }

    public static boolean isPortrait(Activity activity) {
        int rotation = getDisplayRotation(activity);
        return rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180;
    }

    public static int pxToDp(Context context, int px) {
        return px * 160 / context.getResources().getDisplayMetrics().densityDpi;
    }

    public static int dpToPx(Context context, int dp) {
        return dpToPx(context, dp);
    }

    public static int dpToPx(Context context, float dp) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.getResources().getDisplayMetrics());
    }


    //==============================================================================================

    private DisplayUtils() {}
}
